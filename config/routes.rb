Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :products do
      resources :tab_suggested_products, only: [:index, :create]
    end
  end
end
