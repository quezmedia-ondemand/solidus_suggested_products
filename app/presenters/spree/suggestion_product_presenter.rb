module Spree
  class SuggestionProductPresenter < SimpleDelegator
    def self.decorate_collection(products)
      products.map { |p| new(p) }
    end

    def object
      __getobj__
    end

    def suggested_products_by_taxon
      object.taxon_products.uniq.each do |taxon_product|
        related_object = object.suggested_products_by_taxon
          .find_or_initialize_by(related_id: taxon_product.id)
      end
      object.suggested_products_by_taxon
    end

    def suggested_products_without_taxon
      object.suggested_products_without_taxon.group_by(&:related).keys
    end
  end
end
