Deface::Override.new(:virtual_path => "spree/admin/products/_form",
                    :name => "use_all_taxons_products",
                    :insert_after => "div[data-hook='admin_product_form_promotionable']",
                    :partial => "spree/admin/products/edit_use_all_taxons_products_field",
                    :original => '7c6353dc138c5b9f86d50f3f70976549331df2d7',
                    :disabled => false)

Deface::Override.new(:virtual_path => "spree/products/show",
                    :name => "product_suggested_products",
                    :insert_after => "div[data-hook='product_description']",
                    :partial => "spree/products/suggested_products",
                    :original => '5a12f88a1045487cb2618fb558bd73e7b8255dfb',
                    :disabled => false)
