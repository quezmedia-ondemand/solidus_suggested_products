Deface::Override.new(:virtual_path => "spree/admin/shared/_product_tabs",
                     :name => "suggested_products_admin_product_tabs",
                     :insert_bottom => "[data-hook='admin_product_tabs']",
                     :partial => "spree/admin/shared/suggested_products_product_tabs",
                     :original => 'a3b480f18904f1767c610bf9d1f1836c08b0c2a3',
                     :disabled => false)
