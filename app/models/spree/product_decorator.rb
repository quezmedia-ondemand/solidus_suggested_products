module Spree
  Product.class_eval do
    before_validation :set_taxons_products_as_suggested_products,
      if: Proc.new{|p| p.suggesting_taxons_products? }
    has_many :suggested_products,                 ->(){ group(:id, :related_id).order(priority: :desc) },
                                                  dependent: :destroy,
                                                  inverse_of: :product

    has_many :suggested_products_by_taxon,        ->(){ where(coming_from_taxon: true).order(priority: :desc) },
                                                  class_name: Spree::SuggestedProduct,
                                                  dependent: :destroy,
                                                  inverse_of: :product

    has_many :suggested_products_without_taxon,   ->(){ where.not(coming_from_taxon: true).order(priority: :desc) },
                                                  class_name: Spree::SuggestedProduct,
                                                  dependent: :destroy,
                                                  inverse_of: :product

    has_many :suggested,                          class_name: Spree::Product,
                                                  through: :suggested_products,
                                                  source: :related
    has_many :suggestion,                         class_name: Spree::Product,
                                                  through: :suggested_products,
                                                  source: :product


    has_many :products_without_taxon,             class_name: Spree::Product,
                                                  through: :suggested_products_without_taxon,
                                                  source: :related

    has_many :products_with_taxon,                class_name: Spree::Product,
                                                  through: :suggested_products_by_taxon,
                                                  source: :related

    has_many :taxon_products,                     ->(object){ where.not(id: object.id) },
                                                  class_name: Spree::Product,
                                                  through: :taxons,
                                                  source: :products

    accepts_nested_attributes_for :suggested_products, allow_destroy: true, reject_if: :all_blank
    accepts_nested_attributes_for :suggested_products_by_taxon, allow_destroy: true, reject_if: :all_blank
    accepts_nested_attributes_for :suggested_products_without_taxon, allow_destroy: true, reject_if: :all_blank

    private
    def set_taxons_products_as_suggested_products
      unless taxon_product_ids.size == suggested_products_by_taxon.pluck(:related_id).uniq.size
        taxon_product_ids.each do |taxon_product_id|
          suggested_products_by_taxon.find_or_initialize_by(related_id: taxon_product_id)
        end
      end
    end
  end
end
