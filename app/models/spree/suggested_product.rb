module Spree
  class SuggestedProduct < Spree::Base
    after_save :touch_both_product_and_related
    belongs_to :product,             class_name: Spree::Product,
                                     inverse_of: :suggested_products,
                                     required: true
    belongs_to :related,             class_name: Spree::Product,
                                     inverse_of: :suggested_products,
                                     required: true

    default_scope { order(priority: :desc) }

    validates :product_id, uniqueness: { scope: :related_id }

    private
    def touch_both_product_and_related
      [product, related].each{|p| p.update_column(:updated_at, Time.current) }
    end
  end
end
