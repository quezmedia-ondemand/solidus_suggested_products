module Spree
  module Admin
    module SuggestableBehaviors
      extend ActiveSupport::Concern

      def general_suggested_attributes
        [:id, :_destroy, :priority, :related_id, :product_id, :coming_from_taxon]
      end

      def hash_suggested_attributes
        {suggested_products_attributes: general_suggested_attributes,
         suggested_products_by_taxon_attributes: general_suggested_attributes,
         suggested_products_without_taxon_attributes: general_suggested_attributes}
      end

      def suggested_attributes
        [hash_suggested_attributes,
          suggested_attributes: [], suggested_ids: []
        ]
      end
    end
  end
end
