module Spree
  module Admin
    class TabSuggestedProductsController < Spree::Admin::BaseController
      include SuggestableBehaviors
      before_action :find_product

      def index
      end

      # It actually updates
      def create
        if @product.update(product_suggestable_params)
          respond_with(@product) do |format|
            format.html { redirect_to(
              spree.admin_product_tab_suggested_products_path(product_id: @product.id),
              success: Spree.t(:suggestion_updated, scope: [:admin, :suggested_products]))
            }
          end
        else
          render :index
        end
      end

      private
      def find_product
        @product = Spree::Product.friendly.find(params[:product_id])
      end

      def product_suggestable_params
        params.require(:product).permit(suggested_attributes)
      end
    end
  end
end
