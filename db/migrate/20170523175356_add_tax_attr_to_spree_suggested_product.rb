class AddTaxAttrToSpreeSuggestedProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_suggested_products, :coming_from_taxon, :boolean, default: false
  end
end
