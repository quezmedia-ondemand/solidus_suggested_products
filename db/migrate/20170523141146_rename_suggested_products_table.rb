class RenameSuggestedProductsTable < ActiveRecord::Migration[5.0]
  def change
    rename_table :suggested_products, :spree_suggested_products
  end
end
