class CreateSuggestedProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :suggested_products do |t|
      t.references :product, index: true, foreign_key: {to_table: :spree_products}
      t.references :related, index: true, foreign_key: {to_table: :spree_products}
      t.integer :priority, default: 0

      t.timestamps
    end
  end
end
