class AddSAttrToSpreeProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_products, :suggesting_taxons_products, :boolean, default: false
  end
end
